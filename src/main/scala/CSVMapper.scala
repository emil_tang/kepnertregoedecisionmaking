import scala.io.Source

object CSVMapper {
  def read[T](filename: String, mappingFunction: Array[String] => T): List[T] = {
    Source.fromFile(filename).getLines().map(str => str.split(',')).map(mappingFunction).toList
  }
}
