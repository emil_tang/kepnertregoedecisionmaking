case class Alternative(title: String, data: Array[Int]) {
  def score(weights: Array[Int]): Int = Alternative.dot(this.data, weights)
}

case object Alternative {
  private def dot(a: Array[Int], b: Array[Int]): Int = {
    a.zip(b).map(x => x._1 * x._2).sum
  }
}

