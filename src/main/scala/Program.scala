import scala.io.StdIn

object Program extends App {
  override def main(args: Array[String]): Unit = {

    /* Create list of parameter objects from a csv file */
    val paramFileName = StdIn.readLine("Enter parameters file >")
    val mapParam = (line: Array[String]) => Parameter(title = line(0), weight = line(1).toInt)
    val params = CSVMapper.read(paramFileName, mapParam)

    /* Create list of alternatives from a csv file */
    val altsFileName = StdIn.readLine("Enter alternatives file >")
    val mapAlts = (line: Array[String]) => Alternative(title = line(0), data = line.drop(1).map(_.toInt))
    val alternatives = CSVMapper.read(altsFileName, mapAlts)

    val paramValues = params.map(_.weight).toArray

    println()

    alternatives.foreach(a => println(s"${a.title}, ${a.data.mkString(",")}, ${a.score(paramValues)}"))

    /* Find the best alternative */
    val winner = alternatives.maxBy(a => a.score(paramValues))

    println()

    println(s"Best is ${winner.title}")
  }
}